# [2.0.0](https://gitlab.com/SlayerDF/semver-demo/compare/v1.2.0...v2.0.0) (2022-09-07)


### Features

* **core:** :boom: Add a breaking change feature ([b2f5912](https://gitlab.com/SlayerDF/semver-demo/commit/b2f5912e4334a109ffc51deca15c57ed13c810b8))


### BREAKING CHANGES

* **core:** some description of the breaking change

SV-7

# [1.2.0](https://gitlab.com/SlayerDF/semver-demo/compare/v1.1.0...v1.2.0) (2022-09-07)


### Features

* **core:** :boom: Add a breaking change feature ([4c82ece](https://gitlab.com/SlayerDF/semver-demo/commit/4c82ecefc6b27b8783247567e9aa355b7f029e74))

# [1.1.0](https://gitlab.com/SlayerDF/semver-demo/compare/v1.0.1...v1.1.0) (2022-09-07)


### Features

* **core:** :zap: Add a new feature ([10898e3](https://gitlab.com/SlayerDF/semver-demo/commit/10898e3b77cf1ea200023a845c31ed77c127bf48))

## [1.0.1](https://gitlab.com/SlayerDF/semver-demo/compare/v1.0.0...v1.0.1) (2022-09-07)


### Bug Fixes

* **core:** :bug: Fix a bug ([035bede](https://gitlab.com/SlayerDF/semver-demo/commit/035bede1108aad4873c431d9ea5e10ebc19c91fa))

# 1.0.0 (2022-09-07)


### Features

* **core:** :sparkles: Add project API ([6c7d533](https://gitlab.com/SlayerDF/semver-demo/commit/6c7d533fc1acd0422eabe6bed93aac0d3a9c3c91))
